package com.student.model.dao;

public class StudentResult {
	int id;
	private String fname;
	private String subject;
	private String city;
	private String state;
	private boolean status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public StudentResult(int id, String fname, String subject, String city, String state, boolean b) {
		
		this.id = id;
		this.fname = fname;
		this.subject = subject;
		this.city = city;
		this.state = state;
		this.status = b;
	}
	public StudentResult() {
		
	}
	
	
	
	
	
	

}
