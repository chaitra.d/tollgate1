package com.student.model.dao;

import java.util.*;
import java.util.Map.Entry;

public class Tester {
	public static void main(String[] args) {
		StudentDao sdao = new StudentDaoImpl();
		
		List<Student> studentList = sdao.getAllStudents();
	
		
		Map<Integer,String> smap = new HashMap<Integer,String>();
		smap.put(20, "ram");
		smap.put(30, "bham");
		smap.put(40, "om");
		smap.put(50, "sonu");
		smap.put(60, "ann");
		Set<Entry<Integer,String>> es = smap.entrySet();
		System.out.println("results as per marks");
		es.stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		
		
		System.out.println("results as per names");
		es.stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);
		
		//smap.forEach((mark,name)-> System.out.println(mark + ":" + name));
	

}
}
